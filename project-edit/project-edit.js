import * as rightsFile from '../js/right.js';
const rights = rightsFile.rights;

import * as userFile from '../js/userDatas.js';
const user = userFile.user();

const formProject = new Vue({
    el: '#formproject',
    data: {
        errors: [],
        name: '',
        desc: '',
        color: '',
        id: ''
    },
    methods: {
        checkFormProject: function (e) {
            e.preventDefault();
            if (!rights.project.edit) {
                this.errors.push('Vous n\'avez pas les droits!');
                return false;
            }
            if (this.desc && this.name && this.color) {
                let project = {
                    id: this.id,
                    name: this.name,
                    desc: this.desc,
                    prefs_background: this.color
                }
                edit(project)
                return true;
            }
            this.errors = [];
            if (!this.name) {
                this.errors.push('Le projet doit avoir un nom!');
            }
            if (!this.desc) {
                this.errors.push('Le projet doit avoir une description!');
            }
            if (!this.color) {
                this.errors.push('Le projet doit avoir une couleur!');
            }
            return false;
        }
    }
})

const modal = new Vue({
    el: '#confirmeModal',
    data: {
        message: ''
    }
})


function loadCurentProject() {
    if (user.mode == 'trello') {
        let request = "https://api.trello.com/1/boards/" + getProjectId() + "?token=" + user.token + '&key=' + user.key;
        axios.get(request).then((r)=>{
            let datas = r.data;
            formProject.name = datas.name;
            formProject.desc = datas.desc;
            formProject.color = datas.prefs.background;
            formProject.id = datas.id;
        }).catch((err) => {
            console.log("Load project err : "+err)
        })
    }else {
        let request = "http://"+user.ip+'/projects/'+ getProjectId()+'/?token='+user.token; 
        axios.get(request).then((r)=>{
            let datas = r.data[0];
            formProject.name = datas.name;
            formProject.desc = datas.desc;
            formProject.id = datas.id;
        }).catch((err) => {
            console.log("Load project err : "+err)
        })
    }
}

function getProjectId() {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == 'id') { return pair[1]; }
    }
    return false;
}

loadCurentProject();

function edit(project) {
    let request = '';
    let data = Object.keys(project).map((key) => {
        return encodeURIComponent(key.replace(/_/g, '/')) + '=' + encodeURIComponent(project[key].replace(/_/g, '/'));
    }).join('&');
    if (user.mode == 'trello') {
        request = 'https://api.trello.com/1/boards/' + project.id + '?token=' + user.token + '&key=' + user.key+'&'+data;
        axios.put(request).then((r)=>{
            try {
                if (r.data.id) {
                    modal.message = 'Projet modifié';
                    setTimeout(() => { document.location.href = '/project-list/project-list.html'; }, 2000);
                } else {
                    modal.message = 'Erreur lors de l\'édition';
                }
                $('#confirmeModal').modal('show');
            } catch(e){
            }
        }).catch((err) => {
            console.log("Load project err : "+err)
        })
    } else if (user.mode == 'DCPM') {
        let config = { headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        request = "http://"+user.ip+'/projects/'+project.id+'/?token='+user.token; 
        console.log(request);
        axios.put(request, data, config).then((r)=>{
            try {
                if (r.data) {
                    modal.message = 'Projet modifié';
                    setTimeout(() => { document.location.href = '/project-list/project-list.html'; }, 2000);
                } else {
                    modal.message = 'Erreur lors de l\'édition';
                }
                $('#confirmeModal').modal('show');
            } catch(e){
            }
        }).catch((err) => {
            console.log("Load project err : "+err)
        })
    }
    
}