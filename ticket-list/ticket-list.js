//import * as axios from('axios');

import * as rightsFile from '../js/right.js';
const rights = rightsFile.rights;

import * as userFile from '../js/userDatas.js';
const user = userFile.user();

var ticketEnCours = [
    {
        id: 5,
        name: "T1",
        desc: "azertyuiop",
        dateLastActivity: new Date(),
        delState: false
    },
    {
        id: 5,
        name: "T2",
        desc: "azertyuiop",
        dateLastActivity: new Date(),
        delState: false
    },
    {
        id: 5,
        name: "T3",
        desc: "azertyuiop",
        dateLastActivity: new Date(),
        delState: false
    },
    {
        id: 5,
        name: "T4",
        desc: "azertyuiop",
        dateLastActivity: new Date(),
        delState: false
    },
]

var tabs = new Vue({
    el: "#ticket-list-body",
    data: {
        right: rights,
        tabs: []
    },
    methods: {
        changeList: (event, id) => {
            event.preventDefault();

            let tabActive = null;
            let tabToActivate = null;
            tabs.tabs.forEach(element => {
                if (element.menu.id == id) {
                    tabToActivate = element.menu;
                }
                if (element.menu.active == 'active') {
                    tabActive = element.menu;
                }
            });
            tabActive.active = '';
            tabToActivate.active = 'active';

            let listId = id.replace('#', '');
            let listActive = null;
            let listToActivate = null;
            tabs.tabs.forEach(element => {
                if (element.ticketList.id == listId) {
                    listToActivate = element.ticketList;
                }
                if (element.ticketList.active == 'active') {
                    listActive = element.ticketList;
                }
            });
            listActive.active = '';
            listToActivate.active = 'active';
        },
        editTicket: (event, id) => {
            console.log('edit ' + id);
        },
        moveTicketBack: (event, ticketId) => {
            let tabActual = null;
            let tabToMove = null;
            tabs.tabs.forEach((tab) => {
                let ticket = tab.ticketList.tickets.find(function (ticket) {
                    return ticket.id == ticketId;
                })
                if(ticket){
                    tabActual = tab;
                } else if(!tabActual){
                        tabToMove = tab;
                }
            })
            if(tabActual && tabToMove){
                let ticketToMove = tabActual.ticketList.tickets.find((ticket) =>{
                    return ticket.id == ticketId;
                })
                let request = 'https://api.trello.com/1/cards/'+ticketToMove.id+'?token='+user.token+'&key='+user.key+'&idList='+tabToMove.ticketList.id
                axios.put(request).then((r)=>{
                    tabActual.ticketList.tickets.splice(tabActual.ticketList.tickets.indexOf(ticketToMove), 1);
                    tabToMove.ticketList.tickets.push(ticketToMove)
                }).catch((err) => {
                })
            }
        },
        moveTicketNext: (event, ticketId) => {
            let tabActual = null;
            let tabToMove = null;
            let actualTicket = null;
            tabs.tabs.forEach((tab) => {
                let ticket = tab.ticketList.tickets.find((ticket) => {
                    return ticket.id == ticketId;
                })
                if(ticket){
                    actualTicket = ticket;
                    tabActual = tab;
                } else if(tabActual && !tabToMove){
                        tabToMove = tab;
                }
            })
            if(tabActual && tabToMove && actualTicket){
                let request = 'https://api.trello.com/1/cards/'+actualTicket.id+'?token='+user.token+'&key='+user.key+'&idList='+tabToMove.ticketList.id
                axios.put(request).then((r)=>{
                    tabActual.ticketList.tickets.splice(tabActual.ticketList.tickets.indexOf(actualTicket), 1);
                    tabToMove.ticketList.tickets.push(actualTicket)
                }).catch((err) => {
                })
            }
        },
        deleteTicket: (event) => {
            let id = null;
            tabs.tabs.forEach(element => {
                element.ticketList.tickets.forEach(ticket => {
                    if (ticket.delState == true)
                        id = ticket.id;
                })
            });
            console.log("delete " + id);
        },
        changeToDelState: (event, id) => {
            tabs.tabs.forEach(element => {
                element.ticketList.tickets.forEach(ticket => {
                    if (ticket.id == id) {
                        console.log("update "+id);
                        ticket.delState = true;
                    }
                })
            });
        },
        removeAllDelState: (event) => {
            tabs.tabs.forEach(element => {
                element.ticketList.tickets.forEach(ticket => {
                    console.log("remdel "+ticket.id);
                    ticket.delState = false;
                })
            });
        }
    }
})

function newTicket(event) {
    console.log('new ticket');
}

function getProjectId() {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == 'id') { return pair[1]; }
    }
    return false;
}

function loadTabs(projectId){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
            let firstActive = false;
            let jsonLists = JSON.parse(this.response);
            jsonLists.forEach(p => {
                if(!p.closed){
                    let list = {
                        menu: {
                            id: '#'+p.id,
                            name: p.name,
                            active: ''
                        },
                        ticketList: {
                            id: p.id,
                            active: '',
                            tickets: []
                        }
                    }
                    if(!firstActive){
                        list.menu.active = 'active';
                        list.ticketList.active = 'active';
                        console.log("active");
                        firstActive = true;
                    }

                    loadTickets(p.id, list.ticketList.tickets);
                    tabs.tabs.push(list);
                    
                }
            });
        }
    });
    xhr.open("GET", "https://api.trello.com/1/boards/"+projectId+"/lists?token="+user.token+'&key='+user.key);
    xhr.send();
}

function loadTickets(listId, ticketListArray){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
            let jsonTickets = JSON.parse(this.response);
            jsonTickets.forEach((ticket) => {
                ticketListArray.push({
                    id: ticket.id,
                    name: ticket.name,
                    desc: ticket.desc,
                    dateLastActivity: new Date(Date.parse(ticket.dateLastActivity)),
                    delState: false
                })
            })
        }
    });
    xhr.open("GET", "https://api.trello.com/1/lists/"+listId+"/cards?token="+user.token+'&key='+user.key);
    xhr.send();
}

loadTabs(getProjectId());