const formProject = new Vue({
    el: '#formproject',
    data: {
      errors: [],
      name: null,
      desc: null
    },
    methods:{
        checkFormTicket: function (e) {
        if (this.desc && this.name) {
          return true;
        }
        this.errors = [];
        if (!this.name) {
          this.errors.push('Le ticket doit avoir un nom!');
        }
        if (!this.desc) {
          this.errors.push('Le ticket doit avoir une description!');
        }
        e.preventDefault();
      }
    }
  })