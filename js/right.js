import * as userFile from '../js/userDatas.js';
const user = userFile.user();

let right = {}
if(user.mode=='trello'){
    right = {
        project: { show: true, edit: true, create: true, delete: true},
        ticket: { show: true, edit: true, create: true, delete: true},
        group: { show: true, edit: true, create: true, delete: true},
        user: { show: true, edit: true, create: true, delete: true},
        right: { show: true, edit: true, create: true, delete: true},
        list: { show: true, edit: true, create: true, delete: true},
    }
} else if(user.mode == 'DCPM'){
    right = {
        project: { show: true, edit: true, create: true, delete: true},
        ticket: { show: true, edit: true, create: true, delete: true},
        group: { show: true, edit: true, create: true, delete: true},
        user: { show: true, edit: true, create: true, delete: true},
        right: { show: true, edit: true, create: true, delete: true},
        list: { show: true, edit: true, create: true, delete: true},
    }
}

export const rights = right;


