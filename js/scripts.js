/**
 * Load user right
 */
import * as rightsFile from './right.js';
const rights = rightsFile.rights;
/**
 * Header
 */

function addorker(){
   
}

Vue.component('dcpm-header', {
    data: () => {return {right: rights};},
    template: '<nav class="navbar navbar-expand-lg navbar-dark bg-dark">\
    <a class="navbar-brand" href="#">DCPM-project</a>\
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Afficher menu">\
        <span class="navbar-toggler-icon"></span>\
    </button>\
    <div class="collapse" id="navbarNav">\
        <ul class="navbar-nav">\
            <li class="nav-item active" v-if="right.project.show">\
                <a class="nav-link" href="/project-list/project-list.html">Projets</span></a>\
            </li>\
            <li class="nav-item" v-if="right.group.show">\
                <a class="nav-link" href="/group-list/group-list.html">Groups</a>\
            </li>\
            <li class="nav-item" v-if="right.user.show">\
                <a class="nav-link" href="/user-list/user-list.html">Utilisateurs</a>\
            </li>\
            <li class="nav-item" v-if="right.right.show">\
                <a class="nav-link" href="/right-list/right-list.html">Droits</a>\
            </li>\
            <li class="nav-item">\
                <a class="nav-link" href="/deconnexion/deconnexion.html">Déconnexion</a>\
            </li>\
            <li class="nav-item">\
                <a class="nav-link" href="#" v-on:click="addorker($event)">Télécharger l\'app</a>\
            </li>\
        </ul>\
    </div>\
</nav>',
    methods: {
        addorker: (event) => {
            event.preventDefault();
            if('serviceWorker' in navigator) {
                navigator.serviceWorker
                 .register('/js/sw.js')
                 .then(() => {
                    console.log("Service Worker Registered");
                }).catch((err) => {
                    console.log("Eroor PWA "+err);
                })
            }
        }
    }
  })

new Vue({ el: '#header' })
