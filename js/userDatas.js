export const user =()=> {
    let mode = window.localStorage.getItem('Mode');
    if(mode == 'trello'){
        let token =  JSON.parse(window.localStorage.getItem('token'));
        let key =  JSON.parse(window.localStorage.getItem('trello-key'));
        return {
            mode: mode,
            token: token,
            key: key
        }
    } else if(mode == 'DCPM'){
        let ip = JSON.parse(window.localStorage.getItem('ip'));
        let token =  JSON.parse(window.localStorage.getItem('token'));
        return {
            mode: mode,
            ip: ip,
            token: token
        }
    }
}
