import * as rightsFile from '../js/right.js';
const rights = rightsFile.rights;
 
import * as userFile from '../js/userDatas.js';
const user = userFile.user();

const formProject = new Vue({
    el: '#formproject',
    data: {
      errors: [],
      name: null,
      desc: null,
      color: null,
      id: null
    },
    methods:{
        checkFormProject: function (e) {
        e.preventDefault();
        if(!rights.project.create){
          this.errors.push('Vous n\'avez pas les droits!');
          return false;
        }
        if (this.desc && this.name && this.color) {
          let project = {
            name: this.name,
            desc: this.desc,
            prefs_background: this.color
          }
          createProject(project)
          return true;
        }
        this.errors = [];
        if (!this.name) {
          this.errors.push('Le projet doit avoir un nom!');
        }
        if (!this.desc) {
          this.errors.push('Le projet doit avoir une description!');
        }
        if (!this.color) {
          this.errors.push('Le projet doit avoir une couleur!');
        }
        return false;
      }
    }
  })

const modal = new Vue({
  el: '#confirmeModal',
  data: {
    message: ''
  }
})

function createProject(project){
  let data = Object.keys(project).map((key)=>{
    return encodeURIComponent(key.replace(/_/g, '/')) + '=' + encodeURIComponent(project[key].replace(/_/g, '/'));
  }).join('&');
  if(user.mode == 'trello'){
    let request = 'https://api.trello.com/1/boards/?token='+user.token+'&key='+user.key;
    axios.post(request).then((r)=>{
      if(r.data.id){
        modal.message = 'Projet crée';
        setTimeout(() => { document.location.href = '/project-list/project-list.html'; }, 2000);
      }else {
        modal.message = 'Erreur lors de la création';
      }
      $('#confirmeModal').modal('show'); 
    }).catch((err) => {
        modal.message = err;
        $('#confirmeModal').modal('show'); 
    })
  } else if(user.mode == 'DCPM') {
    let config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let request = 'http://'+user.ip+'/projects/?token='+user.token;
    console.log(request);
    axios.post(request, data, config).then((r)=>{
      if(r.data.id){
        modal.message = 'Projet crée';
        setTimeout(() => { document.location.href = '/project-list/project-list.html'; }, 2000);
      }else {
        modal.message = 'Erreur lors de la création';
      }
      $('#confirmeModal').modal('show'); 
    }).catch((err) => {
        modal.message = err;
        $('#confirmeModal').modal('show'); 
    })
  }
  
}