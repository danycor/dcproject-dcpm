import * as rightsFile from '../js/right.js';
const rights = rightsFile.rights;

import * as userFile from '../js/userDatas.js';
const user = userFile.user();

var projects = [];

function loadProject() {
    
    if(user.mode == 'trello'){
        let requestURL = 'https://api.trello.com/1/members/me/boards/?token='+user.token+'&key='+user.key;
        axios.get(requestURL).then((r)=>{
            let datas = r.data;
            datas.forEach(p => {
                if(!p.closed){
                    projects.push({
                        id: p.id,
                        name: p.name,
                        desc: p.desc,
                        dateLastActivity: new Date(Date.parse(p.dateLastActivity)),
                        color: "project-"+p.prefs.background,
                        delState: false
                    });
                }
            });
        }).catch((err) => {
            console.log("Load project err : "+err)
        })
    } else if(user.mode == 'DCPM'){
        var config = {
            headers: {'Access-Control-Allow-Origin': '*'}
        };
        let requestURL = 'http://'+user.ip+'/projects/?token='+user.token;
        axios.get(requestURL, config).then((r) => {
            let datas = r.data;
            console.log(datas);
            datas.forEach(p => {
                projects.push({
                    id: p.id,
                    name: p.name,
                    desc: p.desc,
                    dateLastActivity: new Date(),
                    color: "project-blue",
                    delState: false
                });
            });
        }).catch((err) => {
            console.log("get project error ", err);
        })

    }
}
loadProject();

var projectsList = new Vue({
    el: "#list-project",
    data: {
        right: rights,
        projects: projects
    },
    methods: {
        editProject: (event, id) => {
            if(id && rights.project.edit){
                document.location.href = '/project-edit/project-edit.html?id='+id;
            }
        },
        showProject: (event, id) => {
            event.preventDefault();
            if(id && rights.ticket.show){
                document.location.href = '/ticket-list/ticket-list.html?id='+id;
            }
        },
        deleteProject: (event) => {
            let id = null;
            projectsList.projects.forEach(project => {
                if (project.delState == true)
                    id = project.id;
            });
            if(rights.project.delete){
                let request = '';
                if(user.mode=='trello'){
                    request = 'https://api.trello.com/1/boards/'+id+'?token='+user.token+'&key='+user.key;
                    axios.delete(request).then((r)=>{
                        projectsList.projects.forEach(project => {
                            if (project.id == id)
                                projectsList.projects.pop(project);
                        });
                    }).catch((err) => {
                        console.log("Load project err : "+err)
                    })
                } else if(user.mode=='DCPM'){
                    request = "http://"+user.ip+'/projects/'+id+'/?token='+user.token; 
                    console.log(request);
                    axios.delete(request).then((r)=>{
                        projectsList.projects.forEach(project => {
                            if (project.id == id){
                                projectsList.projects.splice(projectsList.projects.indexOf(project), 1);                                
                            }
                        });
                    }).catch((err) => {
                        console.log("Load project err : "+err)
                    })
                }
            }
        },
        closeAllDelState: (event) => { // Close project
            let id=null;
            projectsList.projects.forEach(project => {
                if (project.delState == true)
                    id = project.id;
            });
            if(rights.project.delete){
                let request = '';
                if(user.mode=='trello'){
                    request = 'https://api.trello.com/1/boards/'+id+'?token='+user.token+'&key='+user.key+'&closed=true';
                    axios.put(request).then((r)=>{
                        projectsList.projects.forEach(project => {
                            if (project.id == id)
                                projectsList.projects.pop(project);
                        });
                    }).catch((err) => {
                        console.log("Load project err : "+err)
                    })
                } else if(user.mode=='DCPM'){
                    projectsList.removeAllDelState();
                }
            }
        },
        changeToDelState: (event, id) => {
            projectsList.projects.forEach(project => {
                if (project.id == id) {
                    project.delState = true;
                }
            });
        },
        removeAllDelState: (event) => {
            projectsList.projects.forEach(project => {
                project.delState = false;
            });
        },
        createProject: (event) => {
            event.preventDefault();
            document.location.href = '/project-create/project-create.html';
        }
    }
})
