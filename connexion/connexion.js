
function DCPMConnexion(e){
    e.preventDefault();
    let login = $('#Login').val();
    let password = $('#password').val();
    let server = $('#ServeurDCPM').val();
    console.log(login+' '+password+' '+server)
}

const connexionForm = new Vue({
    el: '#connexionform',
    data: {
      errors: [],
      key: null,
      token: null,
      ip: null,
      login: null,
      password: null
    },
    methods:{
        checkFormTrello: (e) => {
            e.preventDefault();
            if (connexionForm.key && connexionForm.token) {
                connexionForm.trelloConnexion();
            }
            connexionForm.errors = [];
            if (!connexionForm.key) {
                connexionForm.errors.push('Vous devez ajouter une clé!');
            }
            if (!connexionForm.desc) {
                connexionForm.errors.push('Vous devez ajouter le token! https://trello.com/app-key/');
            }
      },
      trelloConnexion: () => {
            let key = connexionForm.key;
            let token = connexionForm.token;
            let requestUrl = "https://api.trello.com/1/members/me/boards?key="+key+"&token="+token;
        
            axios.get(requestUrl).then((r)=>{
                window.localStorage.setItem('token', JSON.stringify(token));
                window.localStorage.setItem('trello-key', JSON.stringify(key));
                window.localStorage.setItem('Mode', 'trello');
                document.location.href = '../project-list/project-list.html';
            }).catch((err) => {
            })
        },
        checkFormDCPM: (e) => {
            e.preventDefault();
            if (connexionForm.ip && connexionForm.login && connexionForm.password) {
                connexionForm.connexionDCPM();
            }
            connexionForm.errors = [];
            if (!connexionForm.ip) {
                connexionForm.errors.push('Vous devez ajouter l\'adresse du serveur!');
            }
            if (!connexionForm.login) {
                connexionForm.errors.push('Vous devez ajouter votre login!');
            }
            if (!connexionForm.password) {
                connexionForm.errors.push('Vous devez ajouter votre mots de passe!');
            }
      },
      connexionDCPM: () => {
            let user = {
                login: connexionForm.login,
                password: connexionForm.password
            }
            let params = Object.keys(user).map((key) => {
                return key+'='+user[key];
            }).join('&');
            let requestUrl = "http://"+connexionForm.ip+'/login';            
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
            axios.post(requestUrl, params, config).then((r)=>{
                if(r.data.token){
                    window.localStorage.setItem('token', JSON.stringify(r.data.token));
                    window.localStorage.setItem('ip', JSON.stringify(connexionForm.ip));
                    window.localStorage.setItem('Mode', 'DCPM');
                    document.location.href = '../project-list/project-list.html';
                } else {
                    console.log("login or password not valide");
                }
                
                
            }).catch((err) => {
                console.log('Login error: '+err)
            });
        }
    }
  })